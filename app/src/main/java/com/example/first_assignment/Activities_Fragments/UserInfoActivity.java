package com.example.first_assignment.Activities_Fragments;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.first_assignment.DATA_Classes.Constants;
import com.example.first_assignment.DATA_Classes.Model;
import com.example.first_assignment.DATA_Classes.StudentDatabase;
import com.example.first_assignment.R;

public class UserInfoActivity extends AppCompatActivity {

    TextView nameOfuser;
    String firstName,lastName;
    SharedPreferences sharedPreferences;
    static FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    StudentDatabase studentDatabase;
    private  String TAG = "mainActivity";
    private LocalBroadcastManager localBroadcastManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        //typecasting
        nameOfuser = findViewById(R.id.textViewName);
        sharedPreferences = getSharedPreferences(Constants.KEY_MY_PREF,MODE_PRIVATE);
        firstName = sharedPreferences.getString(Constants.KEY_FIRST_NAME,null);
        lastName = sharedPreferences.getString(Constants.KEY_LAST_NAME,null);

        if (firstName != null || lastName != null){
            nameOfuser.setText(firstName+" "+lastName);
        }
        studentListView();      //fragment manager and transaction.
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG,"onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }
    @Override
    protected void onResume() {
        super.onResume();
        //register local broadcast
        IntentFilter intentFilter = new IntentFilter();
        //addAction() method, multiple ACTIONS are added on IntentFilter.
        intentFilter.addAction(Constants.STUDENT_NAVIGATION_ACTION);
        intentFilter.addAction(Constants.STUDENT_ACTION);
        intentFilter.addAction(Constants.CAMERA_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,intentFilter);
        Log.i(TAG,"onResume");

    }
    //Broadcast receiver to receive BroadcastManager.
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //getSerializableExtra() method, get the Serializable class obj.
            Model model = (Model) intent.getSerializableExtra("student_details");

            //getAction() method, get the action which was added on Intent.
            switch (intent.getAction()) {
                case Constants.STUDENT_NAVIGATION_ACTION:
                    //fragment manager and transaction, onclick to StudentDetailsFragment with model class obj.
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container_view, new StudentDetailsFragment(model))
                            .addToBackStack(null)
                            .commit();
                    break;

                case Constants.STUDENT_ACTION:
                    //onClick move to addStudentFragment.
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_container_view, new AddStudentFragment(), null)
                            .addToBackStack(null)
                            .commit();
                    break;
                    //for camera open.
                case Constants.CAMERA_ACTION:
                    Intent intentCam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivity(intentCam);
                    break;
            }
        }
    };


    //user-permission contract either allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.CAMERA_GRANTED) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "permission granted");
                Toast.makeText(this, "Camera Permission Granted", Toast.LENGTH_SHORT).show();
            } else {
                Log.i(TAG, "permission denied");
                Toast.makeText(this, "Camera Permission Denied", Toast.LENGTH_SHORT).show();
            }

        }
    }



    //fragment manager and transaction.
    public void studentListView(){
        fragmentManager = getSupportFragmentManager();
        StudentListFragment studentListFragment = new StudentListFragment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container_view,studentListFragment);
        fragmentTransaction.commit();
    }
}