package com.example.first_assignment.Activities_Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.first_assignment.DATA_Classes.Model;
import com.example.first_assignment.DATA_Classes.StudentDatabase;
import com.example.first_assignment.R;


public class AddStudentFragment extends Fragment {


    private EditText firstname,lastname,city,phoneno;
    private Button saveDetails;
    StudentDatabase studentDatabase;
    Model model = new Model();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_add_student, container, false);

        firstname = view.findViewById(R.id.studentFirstName);
        lastname = view.findViewById(R.id.studentLastName);
        city = view.findViewById(R.id.studentCityName);
        phoneno = view.findViewById(R.id.studentPhoneNo);

        saveDetails = view.findViewById(R.id.saveStudentDetails);

        saveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = firstname.getText().toString();
                String last = lastname.getText().toString();
                String cityName = city.getText().toString();
                String no = phoneno.getText().toString();

                if (name.isEmpty() || last.isEmpty() || cityName.isEmpty() || no.isEmpty()){
                    firstname.setError("Required Field");
                    lastname.setError("Required Field");
                    city.setError("Required Field");
                    phoneno.setError("Required Field");
                }else {
                    model.setName(name);
                    model.setSurname(last);
                    model.setCityName(cityName);
                    model.setPhoneNo(no);

                    studentDatabase = StudentDatabase.getInstance(getContext());
                    studentDatabase.modelDao().addModel(model);
                    //toast of save data
                    Toast.makeText(getContext(), "Data Save Successfully", Toast.LENGTH_SHORT).show();
                    //clear the fill-up fields
                    firstname.setText("");
                    lastname.setText("");
                    city.setText("");
                    phoneno.setText("");


                }
            }
        });
        return view;
    }
}