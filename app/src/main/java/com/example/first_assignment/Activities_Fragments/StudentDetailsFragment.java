package com.example.first_assignment.Activities_Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.first_assignment.DATA_Classes.Model;
import com.example.first_assignment.DATA_Classes.StudentDatabase;
import com.example.first_assignment.R;


public class StudentDetailsFragment extends Fragment implements View.OnClickListener {

    EditText updatedName,updatedSurname,updatedCity,updatedPhoneNo;
    Button updatedDetails;
    StudentDatabase studentDatabase;
    LocalBroadcastManager localBroadcastManager;
    private final Model model;
    //model class constructor.
    public StudentDetailsFragment(Model model) {
        this.model = model;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_details, container, false);

        updatedName = view.findViewById(R.id.updateName);
        updatedSurname = view.findViewById(R.id.updateSurname);
        updatedCity = view.findViewById(R.id.updateCity);
        updatedPhoneNo = view.findViewById(R.id.updatePhoneNo);
        updatedDetails = view.findViewById(R.id.saveUpdates);

        updatedName.setText(model.getName());
        updatedSurname.setText(model.getSurname());
        updatedCity.setText(model.getCityName());
        updatedPhoneNo.setText(model.getPhoneNo());

        updatedDetails.setOnClickListener(this);
         return view;


    }
    @Override
    public void onClick(View v) {

        String upName = updatedName.getText().toString();
        String upSurname = updatedSurname.getText().toString();
        String upCity = updatedCity.getText().toString();
        String upPhoneNo = updatedPhoneNo.getText().toString();

        model.setName(upName);
        model.setSurname(upSurname);
        model.setCityName(upCity);
        model.setPhoneNo(upPhoneNo);
        //initialize database
        studentDatabase = StudentDatabase.getInstance(getContext());
        //update database.
        studentDatabase.modelDao().updateModel(model);
        Toast.makeText(getContext(), "Details Updated!!", Toast.LENGTH_SHORT).show();
    }
}