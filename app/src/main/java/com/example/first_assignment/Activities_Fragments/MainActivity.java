package com.example.first_assignment.Activities_Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.first_assignment.DATA_Classes.Constants;
import com.example.first_assignment.R;

public class MainActivity extends AppCompatActivity {

    static FragmentManager fragmentManager;
    EditText firstName,lastName,userName,password;
    Button logInbtn;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //typecasting
        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        userName = findViewById(R.id.userName);
        password = findViewById(R.id.editTextTextPassword);
        logInbtn = findViewById(R.id.logIn);

        sharedPreferences = getSharedPreferences(Constants.KEY_MY_PREF,MODE_PRIVATE);
        String name = sharedPreferences.getString(Constants.KEY_FIRST_NAME,null);
        String last = sharedPreferences.getString(Constants.KEY_LAST_NAME,null);

        if (name != null && last != null){
            nextActivity(); //userInfoActivity intent
        }

       logInbtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               if (firstName.getText().toString().isEmpty()){
                   firstName.setError("Required");
               }
               else if (lastName.getText().toString().isEmpty()){
                   lastName.setError("Required");
               }
               else if(userName.getText().toString().isEmpty()){
                   userName.setError("Required");
               }
               else if(password.getText().toString().isEmpty()){
                   password.setError("Required");
               }
               else {

                   SharedPreferences.Editor editor = sharedPreferences.edit();
                   editor.putString(String.valueOf(Constants.KEY_FIRST_NAME),firstName.getText().toString());
                   editor.putString(String.valueOf(Constants.KEY_LAST_NAME),lastName.getText().toString());
                   editor.putString(String.valueOf(Constants.KEY_USERNAME), userName.getText().toString());
                   editor.putString(String.valueOf(Constants.KEY_PASSWORD), password.getText().toString());
                   editor.apply();

                   nextActivity();  //userInfoActivity intent

               }


           }
       });
    }

    //intent of userInfoActivity
    protected void nextActivity(){
        Intent i = new Intent(MainActivity.this, UserInfoActivity.class);
        startActivity(i);
        finish();
    }

}