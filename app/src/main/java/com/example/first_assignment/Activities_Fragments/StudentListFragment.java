package com.example.first_assignment.Activities_Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.first_assignment.DATA_Classes.Adapter;
import com.example.first_assignment.DATA_Classes.Basic;
import com.example.first_assignment.DATA_Classes.Constants;
import com.example.first_assignment.DATA_Classes.Model;
import com.example.first_assignment.DATA_Classes.ModelDao;
import com.example.first_assignment.DATA_Classes.StudentDatabase;
import com.example.first_assignment.R;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class StudentListFragment extends Fragment implements View.OnClickListener, Basic {

    private static final String TAG = "IMPORTANT MESSAGE";
    ExtendedFloatingActionButton extendedFABbtn;
    FloatingActionButton fabCamera;
    TextView noDataAvailable;
    private RecyclerView recyclerView;
    private Adapter reAdapter;
    ArrayList<Model> dataholder;
    private ModelDao modelDao;
    private StudentDatabase studentDatabase;
    private LocalBroadcastManager localBroadcastManager;

    private Basic getBasicInstance(){
        return this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG,"onCreateView");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_list, container, false);
        extendedFABbtn = view.findViewById(R.id.floatingAddStudent);
        recyclerView = view.findViewById(R.id.listOfStudents);
        noDataAvailable = view.findViewById(R.id.noRecords);
        fabCamera = view.findViewById(R.id.fabCamera);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        dataholder = new ArrayList<>();
        //initialization of database.
        studentDatabase = StudentDatabase.getInstance(getContext());
        //is database empty
        dataholder = (ArrayList<Model>) studentDatabase.modelDao().isDbEmpty();

        if (dataholder.size() != 0) {
            Log.i(TAG,"DATA PRESENT");
            noDataAvailable.setVisibility(View.GONE);
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        dataholder = (ArrayList<Model>) studentDatabase.modelDao().loadAllData();
                        //runOnUiThread, update UI from non-UI thread
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                reAdapter = new Adapter(dataholder, getBasicInstance());
                                recyclerView.setAdapter(reAdapter);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.start();
        }
        else {
            Log.i(TAG,"NO DATA AVAILABLE");
            noDataAvailable.setVisibility(View.VISIBLE);
        }
        extendedFABbtn.setOnClickListener(this);
        fabCamera.setOnClickListener(this);
        return view;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.floatingAddStudent :
                //localBroadcastManager
                localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
                Intent intent = new Intent(Constants.STUDENT_ACTION);
                localBroadcastManager.sendBroadcast(intent);
            break;

            case R.id.fabCamera :
                checkPermission();
                //code for open camera
            break;

        }
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CAMERA}, Constants.CAMERA_GRANTED);
            Log.i(TAG,"camera ready");
        }
        else {
            Log.i(TAG,"already hava permission");
            localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
            Intent cameraIntent = new Intent(Constants.CAMERA_ACTION);
            localBroadcastManager.sendBroadcast(cameraIntent);
        }
    }
    //onRequestPermissionsResult() method declare in UserInfoActivity.

    @Override
    public void onStudentListItem(Model model) {
        //localBroadcastManager
        localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        Intent localIntent = new Intent(Constants.STUDENT_NAVIGATION_ACTION);
        localIntent.putExtra("student_details", model);
        //send local Broadcast
        localBroadcastManager.sendBroadcast(localIntent);
        Toast.makeText(getContext(), "name "+model.getName(), Toast.LENGTH_SHORT).show();
    }
}