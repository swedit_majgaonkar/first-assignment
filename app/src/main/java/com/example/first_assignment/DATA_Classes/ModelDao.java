package com.example.first_assignment.DATA_Classes;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ModelDao {

    @Insert
    void addModel(Model model);

    @Query("SELECT * FROM student_data")
    List<Model>loadAllData();

    @Update
    void updateModel(Model model);

    @Query("SELECT * FROM student_data")
    List<Model>isDbEmpty();

}
