package com.example.first_assignment.DATA_Classes;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Model.class},  version = 4)

public abstract class StudentDatabase extends RoomDatabase {



    //create database instance
    private static StudentDatabase studentDatabase;

    public static StudentDatabase getInstance(Context context){
        if (studentDatabase == null){
            //define database name
            String DATABASE_NAME = "student_details";
            studentDatabase= Room.databaseBuilder(context.getApplicationContext()
                    ,StudentDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return studentDatabase;
    }
    public abstract ModelDao modelDao();

}
