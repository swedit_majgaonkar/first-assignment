
package com.example.first_assignment.DATA_Classes;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "student_data")
public class Model implements Serializable {

//    private static final long serialVersionUID = 1L;

    @PrimaryKey(autoGenerate = true)
    private int id;
    //Create id column
    @ColumnInfo(name = "studentName")
    private String name;

    @ColumnInfo(name = "studentSurname")
    private String surname;

    @ColumnInfo(name = "cityName")
    private String cityName;

    @ColumnInfo(name = "phoneNo")
    private String  phoneNo;

    //generate getter and setter


    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String  phoneNo) {
        this.phoneNo = phoneNo;
    }
}
