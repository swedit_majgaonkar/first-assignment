package com.example.first_assignment.DATA_Classes;

public class Constants {


    public  static final String KEY_MY_PREF = "MyPreferencesDetails";
    public  static final String KEY_FIRST_NAME = "f_name";
    public  static final String KEY_LAST_NAME = "l_name";
    public  static final String KEY_USERNAME = "u_name";
    public  static final String KEY_PASSWORD = "pass";

    public static final String STUDENT_NAVIGATION_ACTION = "Student navigation action";
    public static final String STUDENT_ACTION = "Student Action";
    public  static final String UPDATE_ACTION = "Information Update";
    public static final int CAMERA_GRANTED = 101;
    public static final String CAMERA_ACTION = "TO CAMERA";




}
