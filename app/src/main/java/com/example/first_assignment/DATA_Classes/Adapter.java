package com.example.first_assignment.DATA_Classes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.first_assignment.R;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.Myviewholder> {

    private final ArrayList<Model> dataholder;
    private final Basic basic;
    public Adapter(ArrayList<Model> dataholder, Basic basic) {
        this.dataholder = dataholder;
        this.basic = basic;
    }
    @NonNull
    @Override
    public Myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_view, parent, false);
        return new Myviewholder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull Myviewholder holder, int position) {
        //initialize main data
        Model model = dataholder.get(position);

        holder.detailsName.setText(model.getName() + " " + model.getSurname());
        holder.itemView.setOnClickListener(v ->{
                    basic.onStudentListItem(model);
                }
        );
    }

    @Override
    public int getItemCount() {

        if (dataholder == null){
            return 0;
        }
        return dataholder.size();
    }

    public class Myviewholder extends RecyclerView.ViewHolder {
        //initialize variable
        TextView detailsName;

        public Myviewholder(@NonNull View itemView) {
            super(itemView);
            //Assign variables
            detailsName = itemView.findViewById(R.id.name);

        }


    }
}



